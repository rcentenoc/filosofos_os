#include <iostream>
#include <mutex>
#include <thread>
using namespace std;

class Fork{//
  public:
    Fork(){;}
    mutex mu;
};

class timer{
  public:
    int time;
    auto endT(float timed){
      cout <<"\nLa cena duro "<< timed <<" S"<<endl;
    }
};

int main(){
  int number_of_philosophers;
  cout<<"Ingrese el numero de filosofos que estarán sentados en la mesa ► ";cin>>number_of_philosophers;cout<<endl;

  timer clock;//Creamos una instancia en nuestro timer
  clock.time=100;//Le damos este tiempo a nuestra instancia para encapsularla en la variable ms
  int ms = clock.time;
  auto start = chrono::high_resolution_clock::now();
  auto eat = [ms](Fork &left_Fork, Fork &right_Fork, int philosopher_number, int stomach=100, int typical_dishes=500){
    while(typical_dishes>0){
    //Creamos dos unique_lock para bloquear los dos  al alzar uno
    if(stomach>100) {
      cout<<"♥ El filosofo "<<philosopher_number<<" ya no puede comer más\n";
      stomach=stomach-100;
      //cout<<"→ El filosofo "<<philosopher_number<<" está comiendo.\n";
    }
    else if(stomach<=0){
      cout<<"# El filosofo "<<philosopher_number<<" murio por inanición\n";
      break;
    }
    else{
        unique_lock<mutex> llock(left_Fork.mu);  
        unique_lock<mutex> rlock(right_Fork.mu); 
        //Una vez que comió ambos  el filosofo empieza a comer
        cout<<"→ El filosofo "<<philosopher_number<<" está comiendo.\n";
        stomach=stomach+100;
        typical_dishes=typical_dishes-100;
        //Esperamos el tiempo encapsulado en la variable ms
        chrono::microseconds timeout(ms);
        this_thread::sleep_for(timeout);
      }
      //La variable ms dicta el tiempo que un filosofo termina de comer  
      if(typical_dishes<=0){
        cout<<"♦ El filosofo " <<philosopher_number<<" termino de comer.\n";
        break;
      }
      cout<<"○ El filosofo " <<philosopher_number<<" termino de comer una porcion.\n";
    }
  
  };
  //creamos un par de arrays para manejar la secuencia de filosofos
  Fork chp[number_of_philosophers];//EN BASE AL NRO DE 
  //EL segundo array sera para controlar mediante los threads 
  thread philosopher[number_of_philosophers];//UN THREAD PARA CADA FILOSOFO
  
  cout<<"☺ El filosofo 1 está pensando.\n";
  //Indicamos que el primer filosofo en comer va tomar un tenedor y el de la izquierda
  philosopher[0] =thread(eat, ref(chp[0]),ref(chp[number_of_philosophers-1]), 1);
  
  for (int i = 1;i < number_of_philosophers;++i){//empieza la rueda, inicializando todos pensando
    cout<<"☺ El filosofo "<<(i+1)<<" está pensando.\n";
    philosopher[i] = thread(eat,ref(chp[i]), ref(chp[i-1]),(i+1));
  }
  //INICIALIZA LOS HILOS DE CADA FILOSOFO
  for (auto &ph: philosopher){
    ph.join();
  }
  //PARAMOS EL RELOJ
  auto end = chrono::high_resolution_clock::now();

  chrono::duration <float> duration = end - start;
  clock.endT(duration.count());
  return 0;
}